# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 14:26:28 2016

@author: drion
"""

import re


try:
    with open('/home/drion/Documents/python/Classes/Lesson_7/logfile', 'r') as f:
        access, res_404 = 0, 0 # All access from the 75.98.230.254 and count of 404 page responce
        status_code = re.compile(r'(?<=") ([0-9]{3}) (?=[0-9]{0,10})') # RegEx to get status code from string
        
        for line in f:
            data = [line.strip().split()[0]] # Get IP address
            data.append(int(status_code.search(line).group())) # Get status code
            
            if data[0] == '75.98.230.254':
                access += 1
                res_404 += (1 if data[1] == 404 else 0)
        
        print 'Total count of access coming from the 75.98.230.254 IP address: %s\nAttempts result in a 404 page response: %s' % (access, res_404)
                    
except IOError:
    print 'No file'