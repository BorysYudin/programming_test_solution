# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 16:27:38 2016

@author: drion
"""

import re


try:
    with open('/home/drion/Documents/python/Classes/Lesson_7/logfile', 'r') as f:
        most_requested_urls = {} # Dict of urls
        status_code = re.compile(r'(?<=") ([0-9]{3}) (?=[0-9]{0,10})') # RegEx to get status code from string
        method = re.compile(r'(?<=] ").*?(?=")') # RegEx to get 'method url HTTP' from string string
        
        for line in f:
            data = [line.strip().split()[0]] #  Get IP address
            data.append(int(status_code.search(line).group())) # Get status code
            data.append(method.search(line).group().split()[0]) # Get method
            try:                                                # Check for url
                url = method.search(line).group().split()[1]
            except IndexError:
                data.append('')
            else:
                data.append(url)
                           
            if data[1] == 404 and data[2] == 'GET':
                most_requested_urls[data[3]] = most_requested_urls.get(data[3], [0,set()])
                most_requested_urls[data[3]][0] += 1
                most_requested_urls[data[3]][1].add(data[0])
    
        print '10 most requested URLs which lead to 404: \n'
        with open('task6.txt', 'w') as rf:
            for position, url in enumerate(sorted(most_requested_urls.items(), key=lambda x: x[1][0], reverse=True)): # Output of urls sorted by dict value
                if position == 10:
                    break
                print '{}. {} - {} times'.format(position + 1, url[0], url[1][0])
                rf.write('{}. {} - {} times \nIPs that request it: {}\n\n'.format(position + 1, url[0], url[1][0], list(url[1][1])))

            print '\nLists of IPs that request URLs are in task6.txt'
            
except IOError:
    print 'No file'