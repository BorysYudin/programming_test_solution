# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 16:14:27 2016

@author: drion
"""

import re
from operator import itemgetter

try:
    with open('/home/drion/Documents/python/Classes/Lesson_7/logfile', 'r') as f:
        address_get404 = {} # Dict of each IP address and the count of 404 apache responses for GET requests
        status_code = re.compile(r'(?<=") ([0-9]{3}) (?=[0-9]{0,10})') # RegEx to get status code from string
        method = re.compile(r'(?<=] ").*?(?=")') # RegEx to get 'method url HTTP' from string string
        
        for line in f:
            data = [line.strip().split()[0]] #  Get IP address
            data.append(int(status_code.search(line).group())) # Get status code
            data.append(method.search(line).group().split()[0]) # Get method
                        
            address_get404[data[0]] = address_get404.get(data[0], 0)   # Add new address to dict if it`s not in dict            
            if data[1] == 404 and data[2] == 'GET':
                address_get404[data[0]] += 1
        
        print 'Top 10 addresses to block(IP - Count of 404 apache responses):'
        for index, address in enumerate(sorted(address_get404.items(), key=itemgetter(1),reverse=True)): # Output of addresses sorted by dict value
            if index == 10: 
                break
            print '%s. %s - %s' % (index + 1, address[0], address[1])
            
except IOError:
    print 'No file'