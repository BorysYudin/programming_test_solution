# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 15:12:56 2016

@author: drion
"""

import re


try:
    with open('/home/drion/Documents/python/Classes/Lesson_7/logfile', 'r') as f:
        adress_get404 = {} # Dict of each IP address and the count of 404 apache responses for GET requests
        status_code = re.compile(r'(?<=") ([0-9]{3}) (?=[0-9]{0,10})') # RegEx to get status code from string
        method = re.compile(r'(?<=] ").*?(?=")') # RegEx to get 'method url HTTP' from string string
        
        for line in f:
            data = [line.strip().split()[0]] # Get IP address
            data.append(int(status_code.search(line).group())) # Get status code
            data.append(method.search(line).group().split()[0]) # Get method
                        
            adress_get404[data[0]] = adress_get404.get(data[0], 0) # Add new address to dict if it`s not in dict          
            if data[1] == 404 and data[2] == 'GET':
                adress_get404[data[0]] += 1
        
        with open('task3.txt', 'w') as rf:
            rf.write('Each IP address and the count of 404 apache responses for GET requests: \n')
            for adress in adress_get404:
                rf.write('%s - %s\n' % (adress, adress_get404[adress]))
        
        print 'Results are saved in task3.txt'
        
except IOError:
    print 'No file'