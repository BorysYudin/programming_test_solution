# -*- coding: utf-8 -*-
"""
Created on Wed Apr 13 15:53:57 2016

@author: drion
"""

import re


try:
    with open('/home/drion/Documents/python/Classes/Lesson_7/logfile', 'r') as f:
        adress_get404_php = {} # Dict of each IP address and the count of 404 apache responses for GET requests of files with file extension .php
        status_code = re.compile(r'(?<=") ([0-9]{3}) (?=[0-9]{0,10})') # RegEx to get status code from string
        method = re.compile(r'(?<=] ").*?(?=")') # RegEx to get 'method url HTTP' from string string
        
        for line in f:
            data = [line.strip().split()[0]] #  Get IP address
            data.append(int(status_code.search(line).group())) # Get status code
            data.append(method.search(line).group().split()[0]) # Get method
            try:                                                # Check for url
                url = method.search(line).group().split()[1]
            except IndexError:
                data.append('')
            else:
                data.append(url)
                
            adress_get404_php[data[0]] = adress_get404_php.get(data[0], 0) # Add new address to dict if it`s not in dict         
            if data[1] == 404 and data[2] == 'GET' and data[3].endswith('.php'):
                adress_get404_php[data[0]] += 1
        
        with open('task4.txt', 'w') as rf:
            rf.write('Each IP address and the count of 404 apache responses for GET requests of files with file extension .php:\n')
            for adress in adress_get404_php:
                rf.write('%s - %s\n' % (adress, adress_get404_php[adress]))
        
        print 'Results are saved in task4.txt'
        
except IOError:
    print 'No file'